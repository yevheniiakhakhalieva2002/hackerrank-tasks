package tasks;

import java.util.Scanner;

public class projectEuler47 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int k = scanner.nextInt();
        findNumbers(n,k);
    }
    private static void findNumbers(int n, int k){
        int[] primes = new int[n+k];
        for (int i = 2; i <= n+k-1; i++){
            if (primes[i] == 0){
                for (int j = i; j <= n+k-1; j += i){
                    primes[j]++;
                }
            }
        }
        int counter = 0;
        for(int i = 2; i <= n+k-1; i++){
            if(k == primes[i]){
                counter++;
                if(k <= counter){
                    System.out.println(i-k+1);
                }
            }else{
                counter = 0;
            }
        }

    }
}
