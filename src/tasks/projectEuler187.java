package tasks;


import java.util.*;

public class projectEuler187 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int testsAm = scanner.nextInt();
        int[] testCases = new int[testsAm];
        int i = 0;
        while(i < testsAm){
            testCases[i] = scanner.nextInt();
            i++;
        }
        for(i = 0; i < testCases.length; i++){
            System.out.println(countSemiprimes(testCases[i]));
        }
    }
    private static int countSemiprimes(int n) {
        int limit = n - 1;
        int count = 0;
        List<Integer> primes = sieveOfEratosthenes(limit/2);
        for (int i = 0; i < primes.size(); i++) {
            if(primes.get(i) <= (int)Math.sqrt(limit)){
                int index = Collections.binarySearch(primes, limit / primes.get(i));
                if (index >= 0){
                    index++;
                }else{
                    index = -index - 1;
                }
                count += index - i;
            }
        }
        return count;
    }
    private static List<Integer> sieveOfEratosthenes(int n) {
        boolean[] isPrime = new boolean[n + 1];
        isPrime[2] = true;
        for (int i = 3; i <= n; i += 2){
            isPrime[i] = true;
        }
        for (int i = 3; i <= (int)Math.sqrt(n); i += 2) {
            if (isPrime[i]) {
                int c = i * 2;
                for (int j = i * i; j <= n; j += c){
                    isPrime[j] = false;
                }
            }
        }
        List<Integer> primeNumbers = new ArrayList<>();
        for (int i = 2; i <= n; i++) {
            if (isPrime[i]) {
                primeNumbers.add(i);
            }
        }
        return primeNumbers;
    }
}
