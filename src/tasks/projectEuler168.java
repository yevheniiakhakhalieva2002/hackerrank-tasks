package tasks;

import java.util.Scanner;

public class projectEuler168 {
    private static final int MODULO = 100000;
    public static void main(String[] args) {
       Scanner scanner = new Scanner(System.in);
       int m = scanner.nextInt();
       System.out.println(calculateSum(m));
    }
    private static int calculateSum(int m){
        if(m < 2 || m > 100){
            return 0;
        }
        int count = 0;
        for (int i = 1; i <= 9; i++){
            for (int n = 2; n <= m; n++){
                for (int j = 1; j <= 9; j++){
                    count += findNumber(i,n,j);
                }

            }
        }
        return count%MODULO;
    }
    private static int findNumber(int x, int numberOfDigits, int lastDigit) {
        int a = 0;
        int forth = 10;
        int currentDigit = lastDigit;
        int properNumber  = currentDigit;
        numberOfDigits--;
        while (numberOfDigits>0){
            int nextDigit = x * currentDigit + a;
            a = nextDigit/10;
            currentDigit = nextDigit%10;
            if (forth < MODULO) {
                properNumber += currentDigit * forth;
                forth*= 10;
            }
            numberOfDigits--;
        }
        int firstDigit = x * currentDigit + a;
        if (currentDigit == 0 || firstDigit != lastDigit){
            return 0;
        }
        return properNumber;
    }
}
