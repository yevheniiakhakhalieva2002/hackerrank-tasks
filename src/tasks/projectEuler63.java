package tasks;

import java.math.BigInteger;
import java.util.Scanner;

public class projectEuler63 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        powerfulDigitCounts(n);
    }
    private static void powerfulDigitCounts(int n){
        BigInteger end = BigInteger.ONE;
        for (int i = 1; i <= n; i++){
            end = end.multiply(BigInteger.valueOf(10));
        }
        BigInteger start = end.divide(BigInteger.valueOf(10));
        end = end.subtract(BigInteger.ONE);
        for (int base = 1; base <= 9; base++) {
            BigInteger power = BigInteger.valueOf(base);
            for (int i = 1; i < n && power.compareTo(end)<=0; i++){
                power = power.multiply(BigInteger.valueOf(base));
            }
            if (power.compareTo(start)>=0 && power.compareTo(end)<=0) {
                System.out.println(power);
            }
        }
    }

}
