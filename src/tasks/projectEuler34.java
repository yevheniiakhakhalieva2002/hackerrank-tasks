package tasks;

import java.util.Scanner;

public class projectEuler34 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        System.out.println(findSum(n));
    }
    private static int findSum(int n) {
        int sum = 0;
        for (int i = 10; i < n; i++) {
            if (sumOfFactorialDigits(i)%i == 0)
                sum += i;
        }
        return sum;
    }
    private static int sumOfFactorialDigits(int x) {
        int[] factorials = {1, 1, 2, 6, 24, 120, 720, 5040, 40320, 362880};
        int sum = 0;
        while (x != 0) {
            sum += factorials[x % 10];
            x /= 10;
        }
        return sum;
    }
}
