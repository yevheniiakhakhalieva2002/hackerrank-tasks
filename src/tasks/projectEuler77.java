package tasks;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class projectEuler77 {
    public static void main(String[] args) {
        long[] ways = countWays();
        Scanner scanner = new Scanner(System.in);
        int t = scanner.nextInt();
        int[] n = new int[t];
        int i = 0;
        while(i < t){
            n[i] = scanner.nextInt();
            i++;
        }
        for(i = 0; i < n.length; i++){
            System.out.println(ways[n[i]]);
        }
    }
    private static final int maxN = 1000;
    private static long[] countWays(){
        List<Integer> primes = new ArrayList<>();
        long[] ways = new long[maxN+1];
        ways[0] = 1;
        for(int i = 2; i <= maxN; i++){
            boolean isPrime = true;
            for(Integer item : primes){
                if(i % item == 0){
                    isPrime = false;
                    break;
                }
                if(item * item > i){
                    break;
                }
            }
            if(isPrime){
                for(int j = 0; j <= maxN - i; j++){
                    ways[i + j] += ways[j];
                }
                primes.add(i);
            }
        }
        return ways;
    }
}
