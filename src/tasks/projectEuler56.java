package tasks;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class projectEuler56 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        System.out.println(findMaxDigitalSum(n));
    }
    private static int findMaxDigitalSum(int n){
        int max = 0;
        for(int a = 1; a < n; a++) {
            for(int b = 1; b < n; b++) {
                int sum = 0;
                BigInteger number = BigInteger.valueOf(a).pow(b);
                String s = number.toString();
                for(int i = 0; i < s.length(); i++) {
                    sum += Character.getNumericValue(s.charAt(i));
                }
                if(sum > max) {
                    max = sum;
                }
            }
        }
        return max;
    }
}
