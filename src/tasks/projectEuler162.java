package tasks;

import java.math.BigInteger;
import java.util.Scanner;

public class projectEuler162 {
    public static void main(String[] args){
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        System.out.println(hexadecimalNumbers(n));

    }
    private static BigInteger hexadecimalNumbers(int n){
        if(n < 3 || n > 100){
            return BigInteger.valueOf(0);
        }
        BigInteger amount = BigInteger.valueOf(0);
        for (int len = 1; len <= n; len++) {
            BigInteger total = BigInteger.valueOf(16).pow(len - 1).multiply(BigInteger.valueOf(15));
            BigInteger haveNo0 = BigInteger.valueOf(15).pow(len);
            BigInteger haveNo1 = BigInteger.valueOf(15).pow(len - 1).multiply(BigInteger.valueOf(14));
            BigInteger haveNo0And1 = BigInteger.valueOf(14).pow(len);
            BigInteger haveNo1a = BigInteger.valueOf(14).pow(len - 1).multiply(BigInteger.valueOf(13));
            BigInteger haveNo01A = BigInteger.valueOf(13).pow(len);
            BigInteger have01a = total.subtract(haveNo0).subtract(haveNo1).subtract(haveNo1).add(haveNo0And1).add(haveNo1a).add(haveNo0And1).subtract(haveNo01A);

            amount = amount.add(have01a);
        }
        return amount.mod(BigInteger.valueOf(1000000007));
    }

}
