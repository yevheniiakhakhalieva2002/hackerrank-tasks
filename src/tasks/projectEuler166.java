package tasks;

import java.util.Scanner;

public class projectEuler166 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        System.out.println(countWays(n));
    }
    private static int countWays(int max) {
        int[] digits = new int[8];
        int count = 1;
        while (incrementDigit(digits,max)){
            int a = digits[0];
            int b = digits[1];
            int c = digits[2];
            int d = digits[3];
            int e = digits[4];
            int f = digits[5];
            int g = digits[6];
            int h = digits[7];
            int i = b + c + d - e - g;
            if (i < 0 || i > max){
                continue;
            }
            int j = a + b + d - f - h;
            if (j < 0 || j > max){
                continue;
            }
            int k = a + b + c - f - i;
            if (k < 0 || k > max){
                continue;
            }
            int l = a + b + c + d - g - k - h;
            if (l < 0 || l > max){
                continue;
            }
            int m = b + c + d*2 - e - g - h;
            if (m < 0 || m > max){
                continue;
            }
            int n = a + b + c + d - e - m - f;
            if (n < 0 || n > max){
                continue;
            }
            int o = a + c + d - m - k;
            if (o < 0 || o > max){
                continue;
            }
            int p = a + b + c - n - l;
            if (p < 0 || p > max){
                continue;
            }
            count++;
        }
        return count;
    }

    private static boolean incrementDigit(int[] digits,int max) {
        int i = 0;
        while (digits[i] == max) {
            digits[i] = 0;
            i++;
            if (i == digits.length){
                return false;
            }
        }
        digits[i]++;
        return true;
    }
}
