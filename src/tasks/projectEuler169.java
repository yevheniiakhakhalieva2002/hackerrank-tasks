package tasks;

import java.math.BigInteger;
import java.util.*;

public class projectEuler169 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String number = scanner.nextLine();
        BigInteger v = new BigInteger(number);
        System.out.println(calculateWays(v));
    }
    private static Map<BigInteger,Long> ways = new HashMap<>();
    private static long calculateWays(BigInteger v) {
        BigInteger base = BigInteger.valueOf(2);
        long count = 0;
        if (ways.containsKey(v)) {
            return ways.get(v);
        }else if (v.equals(BigInteger.valueOf(0))) {
            count = 1;
            return count;
        }
        if (v.mod(base).equals(BigInteger.valueOf(0))) {
            count += calculateWays(v.divide(base));
            count += calculateWays(v.subtract(base).divide(base));
        } else {
            count += calculateWays(v.subtract(BigInteger.valueOf(1)).divide(base));
        }
        ways.put(v, count);
        return count;
    }
}
