package tasks;

import java.util.Scanner;

public class tribonacci {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int term1 = scanner.nextInt();
        int term2 = scanner.nextInt();
        int term3 = scanner.nextInt();
        int k = scanner.nextInt();
        System.out.println(findNumber(term1,term2,term3,k));
    }
    private static int findNumber(int t1, int t2, int t3, int kth){
        if((t1 <= 0 || t1 > 30) || (t2 <= 0 || t2 > 30) || (t3 <= 0 || t3 > 30)){
            return 0;
        }
        if((t1 % 2 == 0) || (t2 % 2 == 0) || (t3 % 2 == 0)){
            return 0;
        }
        if(kth <= 0 || kth > 350){
            return 0;
        }
        int k = 0;
        int number = 1;
        while(k < kth){
            int term1 = t1;
            int term2 = t2;
            int term3 = t3;
            boolean isDivisible = false;
            boolean check = true;
            number+=2;
            while(check){
                int nextTerm = term1+term2+term3;
                nextTerm%=number;
                if(nextTerm == 0){
                    isDivisible = true;
                    check = false;
                }
                term1 = term2;
                term2 = term3;
                term3 = nextTerm;
                if(term1 == t1 && term2 == t2 && term3 == t3){
                    check = false;
                }
            }
            if(!isDivisible){
                k++;
            }
        }
        return number;
    }
}
