package tasks;

import java.math.BigInteger;
import java.util.Scanner;

public class arithmeticGeometricSequence {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int amOfQueries = scanner.nextInt();
        int i = 0;
        while(i <amOfQueries){
            int a = scanner.nextInt();
            int d = scanner.nextInt();
            int n = scanner.nextInt();
            double x = scanner.nextDouble();
            i++;
            System.out.printf("%.12f",findR(a,d,n,x));
          //  System.out.println(findR(a,d,n,x));
        }
    }
    private static double sn(double r,int a,int d,int n){
        double sum = 0;
        double p = 1;
        for(int k = 1; k<=n; k++){
            sum+=((a-(d*k))*p);
            p*=r;
        }
        return sum;
    }

    private static double findR(int a,int d,int n,double x){
        double r = 0;
        double start = 0;
        double end = 1.5;
        while((end-start)>0.00000000000001){
            double mid = ((start+end)/2);
            double currentSum = (sn(mid,a,d,n));

            if((-currentSum) > x){
                end = mid;
            }else{
                start = mid;
            }
            r = mid;
        }
        return r;
    }
}
