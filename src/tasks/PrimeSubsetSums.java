package tasks;

import java.util.Scanner;

public class PrimeSubsetSums {
    private static final long MODULO = 10000000000000000L;
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int amOfQueries = scanner.nextInt();
        int[] values = new int[amOfQueries];
        int i = 0;
        while(i < amOfQueries){
            values[i] = scanner.nextInt();
            i++;
        }
        for(i = 0; i<values.length; i++){
            System.out.print(numberOfSubsets(values[i])+" ");
        }
    }
    private static boolean isPrime(int number){
        if(number <= 1){
            return false;
        }
        else if(number == 2){
            return true;
        }else if(number % 2 == 0){
            return false;
        }
        for(int i = 3; i <= (int)Math.sqrt(number); i+=2){
            if(number % i == 0){
                return false;
            }
        }
        return true;
    }
    private static long numberOfSubsets(int n){
        int maxSum = 0;
        for(int i = 0; i < n; i++){
            if(isPrime(i)){
                maxSum+=i;
            }
        }
        long[] amOfSubsets = new long[maxSum+1];
        amOfSubsets[0] = 1;
        int sum = 0;
        for(int i = 0; i < n; i++){
            if(isPrime(i)){
                sum += i;
                for(int j = sum; j>= i; j--){
                    amOfSubsets[j] += amOfSubsets[j-i];
                    amOfSubsets[j] %= MODULO;
                }
            }
        }
        long totalSubsetsAmount = 0;
        for(int i = 0; i < amOfSubsets.length; i++){
            if(isPrime(i)){
                totalSubsetsAmount += amOfSubsets[i];
                totalSubsetsAmount %=MODULO;
            }
        }
        return totalSubsetsAmount;
    }

}
