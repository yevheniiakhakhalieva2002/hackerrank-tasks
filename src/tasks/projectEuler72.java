package tasks;

import java.util.Scanner;

public class projectEuler72 {
    public static void main(String[] args) {
        long[] sums = countingFractions();
        Scanner scanner = new Scanner(System.in);
        int t = scanner.nextInt();
        int[] testCases = new int[t];
        int i = 0;
        while(i < t){
            testCases[i] = scanner.nextInt();
            i++;
        }
        for(i = 0; i < testCases.length; i++){
            System.out.println(sums[testCases[i]]);
        }

    }
    private static long[] countingFractions(){
        int n = 1000000;
        int[] f = new int[n+1];
        for(int i = 0; i < f.length; i++){
            f[i] = i;
        }
        for(int i = 2; i<=n; i++){
            if(f[i] != i){
                continue;
            }
            for (int j = 1; i * j <= n; j++){
                f[i * j] -= f[i * j] / i;
            }
        }
        long[] sums = new long[f.length];
        for (int i = 2; i <= n; i++){
            sums[i] = sums[i - 1] + f[i];
        }
        return sums;
    }
}
