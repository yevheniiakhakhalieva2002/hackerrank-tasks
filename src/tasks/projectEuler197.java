package tasks;

import java.util.Scanner;

public class projectEuler197 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double u = scanner.nextDouble();
        double b = scanner.nextDouble();
        System.out.printf("%.9f",calculate(u,b));
    }
    private static double calculate(double u0,double b){
        if(u0 < 0 || u0 > 10){
            return 0;
        }
        if(b < 28 || b > 32){
            return 0;
        }
        double u = u0;
        double u2 = f(u,b);
        long i = 0L;
        while(i < 1000000000000L){
            if(i > 0 && u == f(u2,b)){
                break;
            }
            u = u2;
            u2 = f(u,b);
            i++;
            System.out.println(i + "  " + u + "   " + u2);
        }
        return u + u2;
    }
    private static double f(double u,double b){
        return Math.floor(Math.pow(2,b-u*u))/1000000000;
    }
}
