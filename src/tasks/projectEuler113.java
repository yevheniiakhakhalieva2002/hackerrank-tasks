package tasks;

import java.math.BigInteger;
import java.util.Scanner;


public class projectEuler113 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int queries = scanner.nextInt();
        int[] k = new int[queries];
        int i = 0;
        while(i < queries){
            k[i] = scanner.nextInt();
            i++;
        }
        for(i = 0; i<k.length;i++){
            System.out.println(nonBouncyNumbers(k[i]));
        }
    }
    private static final long MODULO = 1000000007;

    private static BigInteger nonBouncyNumbers(int k) {
        BigInteger increasing = binomialCoeff(k + 9, 9).subtract(BigInteger.ONE);
        BigInteger decreasing = binomialCoeff(k + 10, 10).subtract(BigInteger.valueOf(k + 1));
        BigInteger non = BigInteger.valueOf(k * 9);
        return increasing.add(decreasing).subtract(non).mod(BigInteger.valueOf(MODULO));
    }
    private static BigInteger binomialCoeff(int n, int k) {
        BigInteger[] c = new BigInteger[k + 1];
        c[0] = BigInteger.ONE;
        for(int i = 1; i<c.length; i++){
            c[i] = BigInteger.ZERO;
        }
        for (int i = 1; i <= n; i++) {
            for (int j = Math.min(i, k); j > 0; j--)
                c[j] = c[j].add(c[j - 1]);
        }
        return c[k];
    }

}
