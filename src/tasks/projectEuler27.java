package tasks;

import java.util.Scanner;

public class projectEuler27 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        findCoefficients(n);
    }
    private static void findCoefficients(int n) {
        int maxA = 0;
        int maxB = 0;
        int maxAm = 0;
        for (int a = -n; a <= n; a++) {
            for (int b = -n; b <= n; b++) {
                int amount = countAmOfConsecutivePrimeNumbers(a, b);
                if (amount > maxAm) {
                    maxAm = amount;
                    maxA = a;
                    maxB = b;
                }
            }
        }
        System.out.println(maxA + " " + maxB);
    }


    private static int countAmOfConsecutivePrimeNumbers(int a, int b) {
        int i = 0;
        while(true){
            int i1 = i * i + i * a + b;
            if (i1 < 0 || !isPrime(i1)){
                return i;
            }
            i++;
        }
    }
    private static boolean isPrime(int number){
        if(number <= 1){
            return false;
        }
        else if(number == 2){
            return true;
        }else if(number % 2 == 0){
            return false;
        }
        for(int i = 3; i <= (int)Math.sqrt(number); i+=2){
            if(number % i == 0){
                return false;
            }
        }
        return true;
    }
}
