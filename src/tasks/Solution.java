package tasks;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Solution {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int amOfQueries = scanner.nextInt();
        int i = 0;
        while(i <amOfQueries){
            String a = scanner.next();
            String b = scanner.next();
            long n = scanner.nextLong();
            i++;
            System.out.println(findDigitInTerm(a,b,BigInteger.valueOf(n)));
        }
    }
    private static void fillFibonacciList(List<BigInteger> fibonacci,long lengthA, long lengthB, BigInteger n){
        BigInteger firstTerm = BigInteger.valueOf(lengthA);
        BigInteger secondTerm = BigInteger.valueOf(lengthB);
        fibonacci.add(firstTerm);
        fibonacci.add(secondTerm);
        BigInteger nextTerm;
        while(secondTerm.compareTo(n)<0){
            nextTerm = firstTerm.add(secondTerm);
            fibonacci.add(nextTerm);
            firstTerm = secondTerm;
            secondTerm = nextTerm;
        }
    }
    private static int findDigitInTerm(String a, String b, BigInteger n){
        List<BigInteger> fibonacci = new ArrayList<>();
        fillFibonacciList(fibonacci,a.length(),b.length(),n);
        int digit;
        int i = fibonacci.size()-1;
        while(i>=0){
            BigInteger currentTerm = fibonacci.get(i);
            if(currentTerm.compareTo(n)<0){
               n = n.subtract(currentTerm);
               i--;
            }else{
                if(i == 0){
                    break;
                }
                i -= 2;
            }
        }
        if(i==0){
            digit = a.charAt(n.intValue()-1);
        }else{
            digit = b.charAt(n.intValue()-1);
        }
        return digit-48;
    }
}
