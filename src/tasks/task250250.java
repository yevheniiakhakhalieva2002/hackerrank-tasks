package tasks;

import java.math.BigInteger;
import java.util.Scanner;

public class task250250 {
    private static final long MODULO = 1000000000;
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        long n = scanner.nextLong();
        int k = scanner.nextInt();
        System.out.println(numberOfSubsets(n,k));
    }
    private static long numberOfSubsets(long n, int k){
        long[] amOfSubsets = new long[k];
        amOfSubsets[0] = 1;
        for(int i = 1; i<= n; i++){
            int remainder = powAndMod(i,i,k);
            long[] temp = new long[k];
            for(int j = 0; j < k; j++){
                int remainder2 = remainder + j;
                remainder2%=k;
                temp[remainder2] = amOfSubsets[j] + amOfSubsets[remainder2];
                temp[remainder2]%=MODULO;
            }
            amOfSubsets = temp;
        }
        return amOfSubsets[0] - 1;
    }

    private static int powAndMod(int a, int b, int m) {
        if (m == 1){
            return 0;
        }
        BigInteger integer = BigInteger.valueOf(a);
        return integer.pow(b).mod(BigInteger.valueOf(m)).intValue();
    }
}
