package tasks;

import java.util.Scanner;

public class SumsOfDigitFactorials {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int amOfQueries = scanner.nextInt();
        while(amOfQueries>0){
            int inputNum = scanner.nextInt();
            int inputModulo = scanner.nextInt();
            amOfQueries--;
            System.out.println(sgn(inputNum,inputModulo));
        }


    }
    private static int sgn(int n,int modulo){
        int sum = 0;
        for(int i=1; i<=n; i++){
            sum += sumOfDigits(gn(i));
        }
        return sum%modulo;
    }
    private static int gn(int n){
        int searchingNumber = 0;
        for(int i=1; i<Integer.MAX_VALUE; i++){
            if(n == sumOfDigits(fn(i))){
                searchingNumber = i;
                break;
            }
        }
        return searchingNumber;
    }
    private static int fn(int n){
        int sum = 0;
        int digit;
        do{
            digit = n%10;
            sum += calculateFactorial(digit);
            n /= 10;
        }while(n > 0);
        return sum;
    }
    private static int sumOfDigits(int n){
        int sum = 0;
        do{
            sum += n%10;
            n /= 10;

        }while(n > 0);
        return sum;
    }
    private static int calculateFactorial(int n){
        if(n == 1 || n == 0){
            return 1;
        }else{
            return n * calculateFactorial(n-1);
        }
    }
}
