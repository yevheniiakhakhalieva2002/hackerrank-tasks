package tasks;

import java.util.BitSet;
import java.util.Scanner;

public class semidivisibleNumbers {

    private static final long MODULO = 1004535809;
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        long L = scanner.nextLong();
        long R = scanner.nextLong();
        System.out.println(sumOfSemidivisibleNumbers(L,R));
    }
    private static boolean isPrime(long number){
        if(number <= 1){
            return false;
        }
        else if(number == 2){
            return true;
        }else if(number % 2 == 0){
            return false;
        }
        for(int i = 3; i <= (int)Math.sqrt(number); i+=2){
            if(number % i == 0){
                return false;
            }
        }
        return true;
    }
    private static long sumOfSemidivisibleNumbers(long left,long right){
        long sum;
        long lps = (long)Math.sqrt(left);
        while(!isPrime(lps)){
            lps--;
        }
        sum = calculateSum(lps,right);
        if(left != lps*lps || left != lps*lps+1){
            sum-=calculateSum(lps,left-1);
        }
        return sum;
    }
    private static long calculateSum(long leftLps,long right){
        long sum = 0;
        long currentPrime = leftLps;
        while (currentPrime*currentPrime <= right){
            long nextPrime = currentPrime + 1;
            while (!isPrime(nextPrime)){
                nextPrime++;
            }
            long start = currentPrime*currentPrime;
            long end   = nextPrime*nextPrime;
            long i = start + currentPrime;
            while(i < end && i <= right){
                sum += i;
                i += currentPrime;
            }
            while (end - nextPrime > right){
                end -= nextPrime;
            }
            i = end - nextPrime;
            while(i > start){
                sum+=i;
                i-=nextPrime;
            }
            long k = currentPrime*nextPrime;
            for (long j = k; j < end; j += k) {
                if (j >= start){
                    sum -= j;
                    sum -= j;
                }
            }
            currentPrime = nextPrime;
        }
        return sum;
    }
}
