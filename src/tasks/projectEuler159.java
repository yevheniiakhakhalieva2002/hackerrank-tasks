package tasks;

import java.util.Scanner;

public class projectEuler159 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int limit = 0;
        int t = scanner.nextInt();
        int[] queries = new int[t];
        for(int i = 0; i< queries.length; i++){
            int n = scanner.nextInt();
            queries[i] = n;
            if(limit < n){
                limit = n;
            }
        }
        mdrsSum(queries,limit);
    }
    private static void mdrsSum(int[] queries, int limit){
        int[] m = new int[limit+1];
        for (int i = 2; i <= limit; ++i) {
            m[i] = root(i);
        }
        for (int i = 2; i <= limit; i++){
            for (int j = 2; i * j <= limit; j++){
                if(j <= i){
                    if (m[i*j] < m[i] + m[j]){
                        m[i*j] = m[i] + m[j];
                    }
                }


            }
        }
        int[] sums = new int[m.length];
        for (int i = 2; i < sums.length; i++){
            sums[i] = m[i] + sums[i-1];
        }
        for (int query : queries) {
            System.out.println(sums[query]);
        }
    }
    private static int root(int x) {
        int res = 0;
        while (x > 0) {
            res += x % 10;
            x /= 10;
        }
        return res >= 10 ? root(res) : res;
    }
}
