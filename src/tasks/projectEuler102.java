package tasks;

import java.util.Scanner;

public class projectEuler102 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int count = 0;
        int n = scanner.nextInt();
        while(n > 0){
            int x1 = scanner.nextInt();
            int y1 = scanner.nextInt();
            int x2 = scanner.nextInt();
            int y2 = scanner.nextInt();
            int x3 = scanner.nextInt();
            int y3 = scanner.nextInt();
            if(isInTriangle(0,0,x1,y1,x2,y2,x3,y3)){
                count++;
            }
            n--;
        }
        System.out.println(count);
    }
    private static double scalarProduct(double x0, double y0, double x1, double y1, double x2, double y2) {
        return (x1 - x2)*(y0 - y1) + (y2 - y1)*(x0 - x1);
    }
    private static boolean isInTriangle(double x0, double y0 , double x1, double y1, double x2, double y2, double x3, double y3) {
        return ((scalarProduct(x0,y0, x1,y1, x2,y2) >= 0) == (scalarProduct(x0,y0, x2,y2, x3,y3) >= 0))
                && ((scalarProduct(x0,y0, x2,y2, x3,y3) >= 0) == (scalarProduct(x0,y0, x3,y3, x1,y1) >= 0));
    }
}
