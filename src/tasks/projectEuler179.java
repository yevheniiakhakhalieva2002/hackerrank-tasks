package tasks;


import java.util.Scanner;

public class projectEuler179 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int queries = scanner.nextInt();
        int[] n = new int[queries];
        int i = 0;
        while(i < queries){
            n[i] = scanner.nextInt();
            i++;
        }
        for(i = 0; i < n.length; i++){
            System.out.println(findNumber(n[i]));
        }

    }
    private static int findNumber(int n) {
        if(n < 3 || n > 10000000){
            return 0;
        }
        int[] numberOfDivisors = new int[n+1];
        for (int i = 2; i < numberOfDivisors.length; i++) {
            if(i * 2 > numberOfDivisors.length){
                break;
            }
            for (int j = i*2; j < numberOfDivisors.length; j += i)
                numberOfDivisors[j]++;
        }
        int[] count = new int[n+1];
        for (int i = 2; i < numberOfDivisors.length-1; i++) {
            count[i] = count[i-1];
            if (numberOfDivisors[i] == numberOfDivisors[i + 1])
                count[i]++;
        }

        return count[n-1];
    }
   /* private static void fillArray(int[] array){
        for(int i = 0; i < array.length; i++){
            if(i != 0 && i != 1){
                array[i] = 2;
            }
        }
    }*/
}
