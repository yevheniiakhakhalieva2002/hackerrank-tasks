package tasks;

import java.util.Scanner;

public class projectEuler64 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        System.out.println(countFractionsWithOddPeriod(n));
    }
    private static int countFractionsWithOddPeriod(int limit){
        int count = 0;
        for(int i = 2; i <= limit; i++) {
            int root = (int) Math.sqrt(i);
            if(root * root != i) {
                int a = root;
                int period = 0;
                int n = 0;
                int d = 1;
                while(a != root * 2) {
                    n = d * a - n;
                    d = (i - n * n) / d;
                    a = (root + n) / d;
                    period++;
                }
                if (period % 2 != 0) {
                    count++;
                }
            }
        }
        return count;
    }

}
